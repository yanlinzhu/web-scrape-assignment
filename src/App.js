import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import RestarauntCard from './Card';
import './app.css';

const { Header, Content, Footer } = Layout;

class App extends Component {
  render() {
    return (
      <Layout className="layout" id="layout-container">
        <Header>
          <div className="logo" >
            <a href="https://www.novaiq.io/">
              <img src={require('./logo.png')} alt="novaiq-logo" id="logo-image"/>
            </a>
          </div>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['1']}
            style={{ lineHeight: '64px' }}
            id="header-menu"
          >
            <Menu.Item key="1">Home</Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: '0 50px', margin: '20px 0' }}> 
          <div style={{ background: '#fff', padding: 24, minHeight:650 }}>
            <RestarauntCard />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          nova IQ ©2019 Created By Eric Zhu, Naman Sharma and Hersh Vakharia
        </Footer>
      </Layout>
    );
  }
}

export default App;
