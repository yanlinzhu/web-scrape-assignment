const proxy = require('http-proxy-middleware');

module.exports = (app) => {
	app.use(proxy('/api', {
	    target: "htt://localhost:5000"
    }))
}