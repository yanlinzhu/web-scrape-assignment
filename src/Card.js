import React, { Component } from 'react';
import axios from 'axios';
import { Card, Rate } from 'antd';

export default class RestarauntCard extends Component {

    state = {
        data: {},
    }

    componentDidMount() {
        axios.get('/api/restaraunts')
            .then((res) => {
                this.setState({ data: res.data})
            })
    }

    // Return the stars with color based on the number review
    renderStars(num) {
        return <Rate allowHalf disabled value={parseFloat(num.replace(/[^0-9.]/g,''))} />
    }

    // Generate a card with many infos for each restaraunt
    displayContent() {
        let totalCards = []
        const { data } = this.state;
        for (let obj in data) {
            totalCards.push(
                <Card type="inner" key={data[obj][0]} title={data[obj][0]} style={{marginBottom: 20 }}>
                    <p>Address: {data[obj][3]}</p>
                    <p>Phone #: {data[obj][2]}</p>
                    <div className="review-section">
                        Review:
                        {this.renderStars(data[obj][4])}
                    </div>
                    <p>Link: <a href={data[obj][1]}>Check out the restaraunt link</a></p>
                </Card>
            )
        }
        return totalCards;
    }

    // Renders a card with nested cards which show restaraunt details
    render() {
        return (
            <Card title="Restaraunt List">
            <p style={{
                fontSize: 14,
                color: 'rgba(0, 0, 0, 0.85)',
                marginBottom: 16,
                fontWeight: 500,
            }}>
                The following restaraunts are the top 5 five high rated restaraunts in NJ according to YELP. 
            </p>
                {this.displayContent()}
            </Card>
        )
    }
}